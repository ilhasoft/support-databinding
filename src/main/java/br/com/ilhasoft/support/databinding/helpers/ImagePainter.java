package br.com.ilhasoft.support.databinding.helpers;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.widget.ImageView;

/**
 * Created by johncordeiro on 21/10/15.
 */
public class ImagePainter {

    public void paintImageView(ImageView imageView, int color) {
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        imageView.setColorFilter(colorFilter);
    }

}
