package br.com.ilhasoft.support.databinding.bindings;

import android.os.Parcel;

/**
 * Created by johncordeiro on 18/10/15.
 */
public class BindableString extends Bindable<String> {

    public void set(String value) {
        if (!equals(this.value, value)) {
            this.value = value;
            notifyChange();
        }
    }

    public boolean isEmpty() {
        return value == null || value.isEmpty();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.value);
    }

    public BindableString() {
    }

    public BindableString(String value) {
        super(value);
    }

    protected BindableString(Parcel in) {
        this.value = in.readString();
    }

    public static final Creator<BindableString> CREATOR = new Creator<BindableString>() {
        public BindableString createFromParcel(Parcel source) {
            return new BindableString(source);
        }

        public BindableString[] newArray(int size) {
            return new BindableString[size];
        }
    };
}
