package br.com.ilhasoft.support.databinding.bindings;

import android.os.Parcelable;

import androidx.databinding.BaseObservable;

/**
 * Created by john-mac on 1/8/16.
 */
public abstract class Bindable<T> extends BaseObservable implements Parcelable {

    protected OnBindableChangedListener<T> onBindableChangedListener;

    protected T value;

    public T get() {
        return value;
    }

    public void set(T value) {
        this.value = value;
        notifyChange();

        if(onBindableChangedListener != null)
            onBindableChangedListener.onBindableChanged(value);
    }

    public Bindable(T value) {
        set(value);
    }

    public Bindable() {}

    public boolean exists() {
        return value != null;
    }

    public boolean equals(T a, T b) {
        return (a == null) ? (b == null) : a.equals(b);
    }

    public void setOnBindableChangedListener(OnBindableChangedListener<T> onBindableChangedListener) {
        this.onBindableChangedListener = onBindableChangedListener;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bindable<?> bindable = (Bindable<?>) o;
        return value != null ? value.equals(bindable.value) : bindable.value == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    public interface OnBindableChangedListener<T> {
        void onBindableChanged(T object);
    }
}
