package br.com.ilhasoft.support.databinding.helpers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import java.util.List;

/**
 * Created by john-mac on 1/9/16.
 */
public class CustomSpinnerAdapter<T> extends BaseAdapter {

    private final List<T> items;
    private final int itemId;
    private final int layout;

    public CustomSpinnerAdapter(List<T> items, int itemId, int layout) {
        this.items = items;
        this.itemId = itemId;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public T getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewDataBinding binding;

        if(convertView == null) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                    , layout, parent, false);

            convertView = binding.getRoot();
            convertView.setTag(binding);
        } else {
            binding = (ViewDataBinding) convertView.getTag();
        }

        binding.setVariable(itemId, getItem(position));
        return convertView;
    }
}
