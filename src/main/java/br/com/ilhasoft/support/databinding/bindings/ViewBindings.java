package br.com.ilhasoft.support.databinding.bindings;

import android.graphics.Color;
import android.view.View;

import androidx.databinding.BindingAdapter;

/**
 * Created by johncordeiro on 09/10/15.
 */
public class ViewBindings {

    @SuppressWarnings("unchecked")
    @BindingAdapter({"backgroundHexColor"})
    public static void bindBackgroundHexColor(View view, String color) {
        view.setBackgroundColor(Color.parseColor(color));
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter({"visible"})
    public static void bindVisible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
