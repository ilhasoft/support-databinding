package br.com.ilhasoft.support.databinding.bindings;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import br.com.ilhasoft.support.databinding.R;
import br.com.ilhasoft.support.databinding.observables.ObservableString;

/**
 * Created by johncordeiro on 18/10/15.
 */
public class EditTextBindings {

    @BindingAdapter({"binding"})
    public static void bindEditText(EditText editText, final BindableString bindableString) {
        if (editText.getTag(R.id.binded) == null) {
            editText.setTag(R.id.binded, true);
            editText.addTextChangedListener(new TextWatcherClass() {
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    bindableString.set(s.toString());
                }
            });
        }
        String newValue = bindableString.get();
        if (!editText.getText().toString().equals(newValue)) {
            editText.setText(newValue);
        }
    }

    @SuppressWarnings("EqualsBetweenInconvertibleTypes")
    @BindingAdapter({"binding"})
    public static void bindEditText(EditText editText, final ObservableString observableString) {
        if (editText.getTag(R.id.binded) == null) {
            editText.setTag(R.id.binded, true);
            editText.addTextChangedListener(new TextWatcherClass() {
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    observableString.set(s.toString());
                }
            });
        }
        if (!observableString.equals(editText.getText().toString())) {
            editText.setText(observableString.get());
        }
    }

    @BindingAdapter({"checkEmpty"})
    public static void bindCheckEmpty(TextView view, Boolean checkEmpty) {
        view.setVisibility(view.getText().length() > 0 ? View.VISIBLE : View.GONE);
    }

    private static class TextWatcherClass implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }
        @Override
        public void afterTextChanged(Editable s) { }
    }


}