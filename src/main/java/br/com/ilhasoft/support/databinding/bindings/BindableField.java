package br.com.ilhasoft.support.databinding.bindings;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by johncordeiro on 18/10/15.
 */
public class BindableField extends Bindable<Parcelable> {

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.value, 0);
    }

    public BindableField() {
    }

    public BindableField(Parcelable value) {
        super(value);
    }

    protected BindableField(Parcel in) {
        this.value = in.readParcelable(Parcelable.class.getClassLoader());
    }

    public static final Creator<BindableField> CREATOR = new Creator<BindableField>() {
        public BindableField createFromParcel(Parcel source) {
            return new BindableField(source);
        }

        public BindableField[] newArray(int size) {
            return new BindableField[size];
        }
    };

    @Override
    public String toString() {
        return exists() ? get().toString() : super.toString();
    }
}
