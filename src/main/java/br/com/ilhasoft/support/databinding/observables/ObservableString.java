package br.com.ilhasoft.support.databinding.observables;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by daniel on 14/02/16.
 */
public class ObservableString extends ObservableField<String> implements Parcelable, Serializable {

    public static final long serialVersionUID = 1L;

    public ObservableString() {
        this("");
    }

    public ObservableString(String value) {
        super(value);
    }

    @Override
    public void set(String value) {
        if (value == null)
            super.set("");
        else
            super.set(value);
    }

    public boolean isEmpty() {
        return get().isEmpty();
    }

    @Override
    public int describeContents() {
        return 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(get());
    }

    public static final Creator<ObservableString> CREATOR = new Creator<ObservableString>() {
        public ObservableString createFromParcel(Parcel source) {
            return new ObservableString(source.readString());
        }
        public ObservableString[] newArray(int size) {
            return new ObservableString[size];
        }
    };

}
