package br.com.ilhasoft.support.databinding.bindings;

import android.widget.CompoundButton;

import androidx.databinding.BindingAdapter;

import br.com.ilhasoft.support.databinding.observables.ObservableBoolean;

/**
 * Created by daniel on 14/02/16.
 */
public class CompoundButtonBindings {

    private CompoundButtonBindings() { }

    @SuppressWarnings("EqualsBetweenInconvertibleTypes")
    @BindingAdapter("android:checked")
    public static void setChecked(CompoundButton compoundButton, final ObservableBoolean observableBoolean) {
        compoundButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                observableBoolean.set(isChecked);
            }
        });
        if (!observableBoolean.equals(compoundButton.isChecked())) {
            compoundButton.setChecked(observableBoolean.get());
        }
    }

}
