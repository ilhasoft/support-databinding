package br.com.ilhasoft.support.databinding.bindings;

import android.os.Parcel;

import androidx.databinding.BindingConversion;

/**
 * Created by johncordeiro on 18/10/15.
 */
public class BindableBoolean extends Bindable<Boolean> {

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.value);
    }

    public static BindableBoolean getValueTrue() {
        return new BindableBoolean(true);
    }

    public static BindableBoolean getValueFalse() {
        return new BindableBoolean(false);
    }

    public BindableBoolean() {
    }

    public BindableBoolean(Boolean value) {
        super(value);
    }

    @BindingConversion
    public static Boolean convertBindableToBoolean(BindableBoolean bindableBoolean) {
        return bindableBoolean.exists() ? bindableBoolean.get() : false;
    }

    protected BindableBoolean(Parcel in) {
        this.value = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<BindableBoolean> CREATOR = new Creator<BindableBoolean>() {
        public BindableBoolean createFromParcel(Parcel source) {
            return new BindableBoolean(source);
        }

        public BindableBoolean[] newArray(int size) {
            return new BindableBoolean[size];
        }
    };
}
