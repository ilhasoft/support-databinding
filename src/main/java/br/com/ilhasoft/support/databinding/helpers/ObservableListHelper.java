package br.com.ilhasoft.support.databinding.helpers;

import androidx.databinding.ObservableArrayList;

import java.util.List;

/**
 * Created by johncordeiro on 04/11/15.
 */
public class ObservableListHelper {

    public <T> void updateObservableArrayList(List<T> objects, ObservableArrayList<T> observableArrayList) {
        removeListFromObservable(objects, observableArrayList);
        addListToObservable(objects, observableArrayList);
    }

    private <T> void addListToObservable(List<T> objects, ObservableArrayList<T> observableArrayList) {
        int violationsSize = objects.size();
        for (int position = 0; position < violationsSize; position++) {
            T violation = objects.get(position);
            if(position < observableArrayList.size()) {
                observableArrayList.set(position, violation);
            } else {
                observableArrayList.add(violation);
            }
        }
    }

    private <T> void removeListFromObservable(List<T> objects, ObservableArrayList<T> observableArrayList) {
        int violationsSize = objects.size();
        int violationObservableListSize = observableArrayList.size();

        for (int position = violationObservableListSize-1; position >= violationsSize; position--) {
            observableArrayList.remove(position);
        }
    }

}
