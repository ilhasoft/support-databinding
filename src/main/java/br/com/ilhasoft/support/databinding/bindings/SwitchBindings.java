package br.com.ilhasoft.support.databinding.bindings;

import android.widget.CompoundButton;

import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.BindingAdapter;

import br.com.ilhasoft.support.databinding.R;

/**
 * Created by johncordeiro on 03/11/15.
 */
public class SwitchBindings {

    @BindingAdapter({"binding"})
    public static void bindSpinnerCompat(SwitchCompat view, final BindableBoolean bindableBoolean) {
        if (view.getTag(R.id.binded) == null) {
            view.setTag(R.id.binded, true);
            view.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    bindableBoolean.set(isChecked);
                }
            });
        }
        Boolean newValue = bindableBoolean.get();
        if (newValue != null && view.isChecked() != newValue) {
            view.setChecked(newValue);
        }
    }

}
