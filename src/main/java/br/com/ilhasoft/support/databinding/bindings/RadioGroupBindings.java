package br.com.ilhasoft.support.databinding.bindings;

import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

import br.com.ilhasoft.support.databinding.R;

/**
 * Created by johncordeiro on 19/10/15.
 */
public class RadioGroupBindings {

    @SuppressWarnings("unchecked")
    @BindingAdapter({"checked"})
    public static void bindVisible(RadioButton view, int checkedId) {
        view.setChecked(view.getId() == checkedId);
    }

    @BindingAdapter({"binding"})
    public static void bindRadioGroup(RadioGroup view, final Bindable bindable) {
        if(view.getTag(R.id.binded) == null) {
            view.setTag(R.id.binded, true);
            view.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    View child = group.findViewById(checkedId);
                    if(child != null && child.getTag(R.id.binding_value) != null) {
                        Bindable radioButtonField = (Bindable)child.getTag(R.id.binding_value);
                        bindable.set(radioButtonField.get());
                    }
                }
            });
        }

        Log.i("RadioGroupBindings", "bindRadioGroup: bindable: " + bindable);
        updateRadioButtonByField(view, bindable);
    }

    @Nullable
    private static void updateRadioButtonByField(RadioGroup view, Bindable bindable) {
        if (bindable == null || !bindable.exists()) {
            view.clearCheck();
            return;
        }

        for (int i = 0; i < view.getChildCount(); i++) {
            View child = view.getChildAt(i);
            Object radioButtonField = child.getTag(R.id.binding_value);
            if (isRadioButtonBindable(child, radioButtonField)) {
                if(radioButtonField.equals(bindable)) {
                    view.check(child.getId());
                }
            }
        }
    }

    private static boolean isRadioButtonBindable(View child, Object radioButtonValue) {
        return child instanceof RadioButton && radioButtonValue != null
        && radioButtonValue instanceof Bindable;
    }

    @BindingAdapter({"value"})
    public static void bindRadioButton(RadioButton view, final Bindable field) {
        view.setTag(R.id.binding_value, field);
    }

}
