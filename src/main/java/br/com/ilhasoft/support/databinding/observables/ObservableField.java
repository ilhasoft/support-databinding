package br.com.ilhasoft.support.databinding.observables;

import androidx.databinding.BaseObservable;

import java.io.Serializable;

/**
 * Created by daniel on 14/02/16.
 */
public class ObservableField<T> extends BaseObservable implements Serializable {

    public static final long serialVersionUID = 1L;

    private T value;

    public ObservableField() { }

    public ObservableField(T value) {
        this.value = value;
    }

    public T get() {
        return value;
    }

    public void set(T value) {
        if (value != this.value) {
            this.value = value;
            notifyChange();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ObservableField) {
            return ObservableField.equals(value, ((ObservableField) o).get());
        } else {
            return ObservableField.equals(value, o);
        }
    }

    public static boolean equals(Object a, Object b) {
        return (a == null) ? (b == null) : a.equals(b);
    }

}
