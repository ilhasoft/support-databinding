package br.com.ilhasoft.support.databinding.bindings;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.squareup.picasso.Picasso;

import br.com.ilhasoft.support.databinding.helpers.ImagePainter;

/**
 * Created by johncordeiro on 09/10/15.
 */
public class ImageBindings {

    @SuppressWarnings("unchecked")
    @BindingAdapter({"imageUrl", "error"})
    public static void loadImage(ImageView view, String url, Drawable error) {
        Picasso.with(view.getContext()).load(url).error(error).into(view);
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter({"imageUrl", "placeholder", "error"})
    public static void loadImage(ImageView view, String url, Drawable placeholder, Drawable error) {
        Picasso.with(view.getContext()).load(url).placeholder(placeholder).error(error).into(view);
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter({"imageUrl", "placeholderRes", "errorRes"})
    public static void loadImage(ImageView view, String url, int placeholder, int error) {
        Picasso.with(view.getContext()).load(url).placeholder(placeholder).error(error).into(view);
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String url) {
        Picasso.with(view.getContext()).load(url).into(view);
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter({"viewColor"})
    public static void bindViewColor(ImageView view, String hexColor) {
        if(hexColor != null) {
            ImagePainter imagePainter = new ImagePainter();
            imagePainter.paintImageView(view, Color.parseColor(hexColor));
        }
    }

}
