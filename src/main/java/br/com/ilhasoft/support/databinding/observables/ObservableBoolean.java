package br.com.ilhasoft.support.databinding.observables;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by daniel on 14/02/16.
 */
public class ObservableBoolean extends ObservableField<Boolean> implements Parcelable, Serializable {

    public static final long serialVersionUID = 1L;

    public ObservableBoolean() {
        this(false);
    }

    public ObservableBoolean(Boolean value) {
        super(value);
    }

    public void toggle() {
        set(!get());
    }

    @Override
    public int describeContents() {
        return 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(get() ? 1 : 0);
    }

    public static final Creator<ObservableBoolean> CREATOR = new Creator<ObservableBoolean>() {
        public ObservableBoolean createFromParcel(Parcel source) {
            return new ObservableBoolean(source.readInt() == 1);
        }
        public ObservableBoolean[] newArray(int size) {
            return new ObservableBoolean[size];
        }
    };

}
