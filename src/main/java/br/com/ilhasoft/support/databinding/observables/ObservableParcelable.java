package br.com.ilhasoft.support.databinding.observables;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by daniel on 14/02/16.
 */
public class ObservableParcelable<T extends Parcelable> extends ObservableField<T>
        implements Parcelable, Serializable {

    public static final long serialVersionUID = 1L;

    public ObservableParcelable() {
        super();
    }

    public ObservableParcelable(T value) {
        super(value);
    }

    @Override
    public int describeContents() {
        return 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(get(), 0);
    }

    public static final Creator<ObservableParcelable> CREATOR = new Creator<ObservableParcelable>() {
        public ObservableParcelable createFromParcel(Parcel source) {
            //noinspection unchecked
            return new ObservableParcelable(source.readParcelable(getClass().getClassLoader()));
        }
        public ObservableParcelable[] newArray(int size) {
            return new ObservableParcelable[size];
        }
    };

}
