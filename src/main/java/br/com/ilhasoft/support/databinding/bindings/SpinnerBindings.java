package br.com.ilhasoft.support.databinding.bindings;

import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import androidx.databinding.BindingAdapter;

import java.util.List;

import br.com.ilhasoft.support.databinding.R;
import br.com.ilhasoft.support.databinding.helpers.CustomSpinnerAdapter;

/**
 * Created by johncordeiro on 18/10/15.
 */
public class SpinnerBindings {

    private static final String TAG = "SpinnerBindings";

    @SuppressWarnings("unchecked")
    @BindingAdapter({"items"})
    public static <T> void setItems(Spinner spinner, List<T> items) {
        if(items != null) {
            ArrayAdapter adapter = new ArrayAdapter<>(spinner.getContext(), android.R.layout.simple_list_item_1, items);
            spinner.setAdapter(adapter);

            addDefaultTextIfNeeded(spinner, adapter);
            bindValueIfNeeded(spinner);
        }
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter({"items", "itemId", "layout"})
    public static <T> void bindItemsCustom(Spinner spinner, final List<T> items, final int itemId, final int layout) {
        if(items != null) {
            BaseAdapter adapter = new CustomSpinnerAdapter<>(items, itemId, layout);
            spinner.setAdapter(adapter);

            bindValueIfNeeded(spinner);
        }
    }

    private static void bindValueIfNeeded(Spinner spinner) {
        Object bindableObject = spinner.getTag(R.id.binding_value);
        if(bindableObject != null) {
            selectForObject(spinner, (BindableField) bindableObject);
        }
    }

    private static void addDefaultTextIfNeeded(Spinner spinner, ArrayAdapter adapter) {
        Object defaultText = spinner.getTag(R.id.spinner_default_text);
        if(defaultText != null) {
            adapter.insert(defaultText, 0);
        }
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter({"defaultText"})
    public static void setDefaultText(Spinner spinner, String defaultText) {
        if(spinner.getTag(R.id.spinner_default_text) == null) {
            spinner.setTag(R.id.spinner_default_text, defaultText);
        }
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter({"binding"})
    public static void bindSpinner(final Spinner spinner, final BindableField bindableObject) {
        if (spinner.getTag(R.id.binded) == null) {
            spinner.setTag(R.id.binded, true);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(!isDefaultText(spinner, position)) {
                        bindableObject.set((Parcelable) parent.getItemAtPosition(position));
                    } else {
                        bindableObject.set(null);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    bindableObject.set(null);
                }
            });
        }

        SpinnerAdapter adapter = spinner.getAdapter();
        if(adapter != null) {
            selectForObject(spinner, bindableObject);
        } else {
            spinner.setTag(R.id.binding_value, bindableObject);
        }
    }

    private static void selectForObject(Spinner spinner, BindableField bindableObject) {
        Parcelable newValue = bindableObject.get();
        for (int position = 0; position < spinner.getCount(); position++) {
            if(spinner.getItemAtPosition(position).equals(newValue)) {
                spinner.setSelection(position);
                break;
            }
        }
    }

    private static boolean isDefaultText(Spinner spinner, int position) {
        Object defaultText = spinner.getTag(R.id.spinner_default_text);
        return defaultText != null && position == 0;
    }

}
